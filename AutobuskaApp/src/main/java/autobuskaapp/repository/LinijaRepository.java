package autobuskaapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import autobuskaapp.model.Linija;


@Repository
public interface LinijaRepository extends JpaRepository<Linija,Long> {
	
	List<Linija> findByDestinacijaIgnoreCaseContainsAndPrevoznikId(String destinacija,Long prevoznikId);
	
	List<Linija> findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThanEqual(String destinacija,Double maxCenaKarte);
	



    List<Linija> findByDestinacijaIgnoreCaseContainsAndPrevoznikIdAndCenaKarteLessThanEqual(String destinacija,Long prevoznikId, Double maxCenaKarte);
	
	

}
