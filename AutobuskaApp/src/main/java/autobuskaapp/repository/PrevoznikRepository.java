package autobuskaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import autobuskaapp.model.Prevoznik;

@Repository
public interface PrevoznikRepository extends JpaRepository<Prevoznik,Long> {

}
