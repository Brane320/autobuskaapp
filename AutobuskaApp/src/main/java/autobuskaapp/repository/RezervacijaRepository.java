package autobuskaapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import autobuskaapp.model.Rezervacija;

@Repository
public interface RezervacijaRepository extends JpaRepository<Rezervacija,Long>{

}
