package autobuskaapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import autobuskaapp.model.Linija;

public interface LinijaService {
	
	Page<Linija> findAll(int brojStranice);
	
	Linija getOne(Long id);
	
	Optional<Linija> findById(Long id);
	
	Linija save(Linija linija);
	
	Linija update(Linija linija);
	
	Linija delete(Long id);
	
	List<Linija> find(String destinacija,Long prevoznikId,Double maxCenaKarte);

}
