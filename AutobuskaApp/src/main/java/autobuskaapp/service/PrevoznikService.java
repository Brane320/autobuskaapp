package autobuskaapp.service;

import java.util.List;
import java.util.Optional;

import autobuskaapp.model.Prevoznik;

public interface PrevoznikService {
	
	List<Prevoznik> getAll();
	
	Prevoznik save(Prevoznik prevoznik);
	
	Optional<Prevoznik> findById(Long id);
	
	Prevoznik getOne(Long id);
	
	

}
