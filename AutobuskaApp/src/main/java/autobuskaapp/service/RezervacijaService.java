package autobuskaapp.service;

import autobuskaapp.model.Rezervacija;

public interface RezervacijaService {

	Rezervacija save(Rezervacija rezervacija);
	
	Rezervacija delete(Long id);
	
	Rezervacija getOne(Long id);
}
