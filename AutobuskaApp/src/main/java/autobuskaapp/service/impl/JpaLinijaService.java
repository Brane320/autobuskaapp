package autobuskaapp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import autobuskaapp.model.Linija;
import autobuskaapp.repository.LinijaRepository;
import autobuskaapp.service.LinijaService;

@Service
public class JpaLinijaService implements LinijaService {
	
	@Autowired
	private LinijaRepository linijaRepository;

	@Override
	public Page<Linija> findAll(int brojStranice) {
		return linijaRepository.findAll(PageRequest.of(brojStranice, 3));
	}

	@Override
	public Linija getOne(Long id) {
		return linijaRepository.getOne(id);
	}

	@Override
	public Optional<Linija> findById(Long id) {
		return linijaRepository.findById(id);
	}

	@Override
	public Linija save(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija update(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija delete(Long id) {
		Optional<Linija> linija = linijaRepository.findById(id);
		
		if(linija.isPresent()) {
			linijaRepository.deleteById(id);
			return linija.get();
		}
		return null;
	}

	@Override
	public List<Linija> find(String destinacija, Long prevoznikId, Double maxCenaKarte) {
		
		
		if(destinacija == null) {
			destinacija = "";
		}
		
		if(maxCenaKarte == null) {
			maxCenaKarte = Double.MAX_VALUE;
		}
		
		if(prevoznikId == null) {
			return linijaRepository.findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThanEqual(destinacija, maxCenaKarte);
		}
		return linijaRepository.findByDestinacijaIgnoreCaseContainsAndPrevoznikIdAndCenaKarteLessThanEqual(destinacija, prevoznikId, maxCenaKarte);
	
		
		
	}

	

}
