package autobuskaapp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autobuskaapp.model.Prevoznik;
import autobuskaapp.repository.PrevoznikRepository;
import autobuskaapp.service.PrevoznikService;
@Service
public class JpaPrevoznikService implements PrevoznikService {
	
	@Autowired
	private PrevoznikRepository prevoznikRepository;

	@Override
	public List<Prevoznik> getAll() {
		return prevoznikRepository.findAll();
	}

	@Override
	public Prevoznik save(Prevoznik prevoznik) {
		return prevoznikRepository.save(prevoznik);
	}

	@Override
	public Optional<Prevoznik> findById(Long id) {
		return prevoznikRepository.findById(id);
	}

	@Override
	public Prevoznik getOne(Long id) {
		return prevoznikRepository.getOne(id);
	}

}
