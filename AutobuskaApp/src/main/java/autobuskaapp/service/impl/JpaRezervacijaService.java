package autobuskaapp.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autobuskaapp.model.Linija;
import autobuskaapp.model.Rezervacija;
import autobuskaapp.repository.RezervacijaRepository;
import autobuskaapp.service.LinijaService;
import autobuskaapp.service.RezervacijaService;

@Service
public class JpaRezervacijaService implements RezervacijaService {
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;
	
	@Autowired
	private LinijaService linijaService;

	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		int brojMestaLinije = rezervacija.getLinija().getBrojMesta();
		int brojLjudiRezervacije = rezervacija.getBrojOsoba();
		
		if(brojMestaLinije >= brojLjudiRezervacije) {
		Linija linija = rezervacija.getLinija();
		linija.setBrojMesta(linija.getBrojMesta() - rezervacija.getBrojOsoba());
		linijaService.update(linija);
		return rezervacijaRepository.save(rezervacija);
		
		}
		return null;
		
	}

	@Override
	public Rezervacija delete(Long id) {
		Optional<Rezervacija> rez = rezervacijaRepository.findById(id);
		
		if(rez.isPresent()) {
			rezervacijaRepository.deleteById(id);
			return rez.get();
		}
		return null;
	}

	@Override
	public Rezervacija getOne(Long id) {
		return rezervacijaRepository.getOne(id);
	}

}
