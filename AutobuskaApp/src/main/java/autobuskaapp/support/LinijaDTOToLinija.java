package autobuskaapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import autobuskaapp.model.Linija;
import autobuskaapp.service.LinijaService;
import autobuskaapp.service.PrevoznikService;
import autobuskaapp.web.dto.LinijaDTO;

@Component
public class LinijaDTOToLinija implements Converter<LinijaDTO,Linija>{
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Autowired
	private LinijaService linijaService;

	@Override
	public Linija convert(LinijaDTO dto) {
		Linija linija;
		
		if(dto.getId() == null) {
			linija = new Linija();
		} else {
			linija = linijaService.getOne(dto.getId());
		}
		
		if(linija != null) {
			linija.setBrojMesta(dto.getBrojMesta());
			linija.setCenaKarte(dto.getCenaKarte());
			linija.setDestinacija(dto.getDestinacija());
			linija.setVremePolaska(dto.getVremePolaska());
			linija.setPrevoznik(prevoznikService.getOne(dto.getPrevoznik().getId()));
			
			
		}
		return linija;
	}

}
