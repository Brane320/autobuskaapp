package autobuskaapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import autobuskaapp.model.Prevoznik;
import autobuskaapp.service.PrevoznikService;
import autobuskaapp.web.dto.PrevoznikDTO;

@Component
public class PrevoznikDTOToPrevoznik implements Converter<PrevoznikDTO,Prevoznik> {
	
	@Autowired
	private PrevoznikService prevoznikService;

	@Override
	public Prevoznik convert(PrevoznikDTO dto) {
		Prevoznik prevoznik;
		
		if(dto.getId() == null) {
			prevoznik = new Prevoznik();
		} else {
			prevoznik = prevoznikService.getOne(dto.getId());
		}
		
		if(prevoznik != null) {
			prevoznik.setAdresa(dto.getAdresa());
			prevoznik.setNaziv(dto.getNaziv());
			prevoznik.setPib(dto.getPib());
			
	}
		return prevoznik;
		
	}

}
