package autobuskaapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import autobuskaapp.model.Prevoznik;
import autobuskaapp.web.dto.PrevoznikDTO;

@Component
public class PrevoznikToPrevoznikDTO implements Converter<Prevoznik,PrevoznikDTO> {

	@Override
	public PrevoznikDTO convert(Prevoznik source) {
		PrevoznikDTO dto = new PrevoznikDTO();
		
		dto.setId(source.getId());
		dto.setAdresa(source.getAdresa());
		dto.setNaziv(source.getNaziv());
		dto.setPib(source.getPib());
		
		return dto;
	}
	
	public List<PrevoznikDTO> convert(List<Prevoznik> prevoznici){
		List<PrevoznikDTO> dto = new ArrayList<>();
		
		for(Prevoznik prevoznik : prevoznici) {
			dto.add(convert(prevoznik));
		}
		return dto;
	}

}
