package autobuskaapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import autobuskaapp.model.Rezervacija;
import autobuskaapp.service.RezervacijaService;
import autobuskaapp.web.dto.RezervacijaDTO;

@Component
public class RezervacijaDTOToRezervacija implements Converter<RezervacijaDTO,Rezervacija> {
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private LinijaDTOToLinija toLinija;

	@Override
	public Rezervacija convert(RezervacijaDTO dto) {
		Rezervacija rezervacija;
		
		if(dto.getId() == null) {
			rezervacija = new Rezervacija();
		} else {
			rezervacija = rezervacijaService.getOne(dto.getId());
		}
		
		if(rezervacija != null) {
			rezervacija.setId(dto.getId());
			rezervacija.setBrojOsoba(dto.getBrojOsoba());
			rezervacija.setLinija(toLinija.convert(dto.getLinija()));
		}
		return rezervacija;
	}

}
