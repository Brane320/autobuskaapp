package autobuskaapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import autobuskaapp.model.Rezervacija;
import autobuskaapp.web.dto.RezervacijaDTO;

@Component
public class RezervacijaToRezervacijaDTO implements Converter<Rezervacija,RezervacijaDTO> {
	@Autowired
	private LinijaToLinijaDTO toLinijaDTO;

	@Override
	public RezervacijaDTO convert(Rezervacija source) {
		RezervacijaDTO dto = new RezervacijaDTO();
		
		dto.setId(source.getId());
		dto.setBrojOsoba(source.getBrojOsoba());
		dto.setLinija(toLinijaDTO.convert(source.getLinija()));
		
		return dto;
	}
	
	public List<RezervacijaDTO> convert(List<Rezervacija> rez){
		List<RezervacijaDTO> dto = new ArrayList<>();
		
		for(Rezervacija rezervacija : rez) {
			dto.add(convert(rezervacija));
		}
		return dto;
	}

}
