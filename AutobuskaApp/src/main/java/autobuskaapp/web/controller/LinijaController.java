package autobuskaapp.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import autobuskaapp.model.Linija;
import autobuskaapp.service.LinijaService;
import autobuskaapp.support.LinijaDTOToLinija;
import autobuskaapp.support.LinijaToLinijaDTO;
import autobuskaapp.web.dto.LinijaDTO;



@RestController
@RequestMapping(value = "/api/linije", produces = MediaType.APPLICATION_JSON_VALUE)
public class LinijaController {
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private LinijaToLinijaDTO toLinijaDTO;
	
	@Autowired
	private LinijaDTOToLinija toLinija;
	
	@GetMapping
	public ResponseEntity<List<LinijaDTO>> getAll(@RequestParam(defaultValue="0") int brojStranice){
		Page<Linija> sve = linijaService.findAll(brojStranice);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Total-Pages", sve.getTotalPages() + "");
		
		return new ResponseEntity<>(toLinijaDTO.convert(sve.getContent()),headers,HttpStatus.OK);
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<LinijaDTO> getOne(@PathVariable Long id){
		Optional<Linija> linija = linijaService.findById(id);
		
		if(linija.isPresent()) {
			return new ResponseEntity<>(toLinijaDTO.convert(linija.get()), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> create(@Valid @RequestBody LinijaDTO linijaDTO){
		Linija linija = toLinija.convert(linijaDTO);
		
		Linija sacuvana = linijaService.save(linija);
		
		
		
		
		return new ResponseEntity<>(toLinijaDTO.convert(sacuvana), HttpStatus.CREATED);
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value="/{id}" , consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> update(@PathVariable Long id, @Valid @RequestBody LinijaDTO dto){
		
		
		if(!dto.getId().equals(id)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Linija linija = toLinija.convert(dto);
		
		if(linija.getPrevoznik() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		
		Linija izmenjena = linijaService.update(linija);
		
		
		
		
		return new ResponseEntity<>(toLinijaDTO.convert(izmenjena), HttpStatus.OK);
		
	}
	
		
	
		
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<LinijaDTO> delete(@PathVariable Long id){
		Linija obrisana = linijaService.delete(id);
		
		if(obrisana != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
					
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	

	  @GetMapping("/pretraga")
		public ResponseEntity<List<LinijaDTO>> pretraga(@RequestParam(required=false) String destinacija, @RequestParam(required=false) Long prevoznikId, @RequestParam(required=false) Double maxCenaKarte){
			List<Linija> rezultat = linijaService.find(destinacija, prevoznikId, maxCenaKarte);
			
			return new ResponseEntity<>(toLinijaDTO.convert(rezultat),HttpStatus.OK);
		}
	
	
			
			
	
	

}
