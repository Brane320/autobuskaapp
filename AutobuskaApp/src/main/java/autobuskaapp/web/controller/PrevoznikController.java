package autobuskaapp.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import autobuskaapp.model.Prevoznik;
import autobuskaapp.service.PrevoznikService;
import autobuskaapp.support.PrevoznikDTOToPrevoznik;
import autobuskaapp.support.PrevoznikToPrevoznikDTO;
import autobuskaapp.web.dto.PrevoznikDTO;


@RestController
@RequestMapping(value = "/api/prevoznici", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrevoznikController {
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Autowired
	private PrevoznikToPrevoznikDTO toPrevoznikDTO;
	
	@Autowired
	private PrevoznikDTOToPrevoznik toPrevoznik;
	
	@GetMapping
	public ResponseEntity<List<PrevoznikDTO>> getAll(){
		List<Prevoznik> svi = prevoznikService.getAll();
		
		return new ResponseEntity<>(toPrevoznikDTO.convert(svi),HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrevoznikDTO> create(@Valid @RequestBody PrevoznikDTO prevoznikDTO){
		Prevoznik prevoznik = toPrevoznik.convert(prevoznikDTO);
		
		Prevoznik sacuvan = prevoznikService.save(prevoznik);
		
		
		
		
		
		
		return new ResponseEntity<>(toPrevoznikDTO.convert(sacuvan), HttpStatus.CREATED);
	}
	

}
