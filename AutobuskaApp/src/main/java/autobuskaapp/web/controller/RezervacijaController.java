package autobuskaapp.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import autobuskaapp.model.Linija;
import autobuskaapp.model.Rezervacija;
import autobuskaapp.service.RezervacijaService;
import autobuskaapp.support.RezervacijaDTOToRezervacija;
import autobuskaapp.support.RezervacijaToRezervacijaDTO;
import autobuskaapp.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/rezervacije", produces = MediaType.APPLICATION_JSON_VALUE)
public class RezervacijaController {
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private RezervacijaDTOToRezervacija toRezervacija;
	
	@Autowired
	private RezervacijaToRezervacijaDTO toRezervacijaDTO;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RezervacijaDTO> create(@Valid @RequestBody RezervacijaDTO rezervacijaDTO){
		Rezervacija rezervacija = toRezervacija.convert(rezervacijaDTO);
		
		Rezervacija sacuvana = rezervacijaService.save(rezervacija);
		
		
		
		
		return new ResponseEntity<>(toRezervacijaDTO.convert(sacuvana), HttpStatus.CREATED);
	}

}
