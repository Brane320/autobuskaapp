package autobuskaapp.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class LinijaDTO {
	
		private Long id;
		
		@Positive
		private int brojMesta;
		
		
		private Double cenaKarte;
		
		
		private String vremePolaska;
		
		@NotEmpty
		@NotNull
		private String destinacija;
		
		
		private PrevoznikDTO prevoznik;


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public int getBrojMesta() {
			return brojMesta;
		}


		public void setBrojMesta(int brojMesta) {
			this.brojMesta = brojMesta;
		}


		public Double getCenaKarte() {
			return cenaKarte;
		}


		public void setCenaKarte(Double cenaKarte) {
			this.cenaKarte = cenaKarte;
		}


		public String getVremePolaska() {
			return vremePolaska;
		}


		public void setVremePolaska(String vremePolaska) {
			this.vremePolaska = vremePolaska;
		}


		public String getDestinacija() {
			return destinacija;
		}


		public void setDestinacija(String destinacija) {
			this.destinacija = destinacija;
		}


		public PrevoznikDTO getPrevoznik() {
			return prevoznik;
		}


		public void setPrevoznik(PrevoznikDTO prevoznik) {
			this.prevoznik = prevoznik;
		}
		
		
	

}
