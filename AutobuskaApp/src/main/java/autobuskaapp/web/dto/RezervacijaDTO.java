package autobuskaapp.web.dto;

public class RezervacijaDTO {
	

    private Long id;
	
	
	private int brojOsoba;
	
	
	private LinijaDTO linija;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getBrojOsoba() {
		return brojOsoba;
	}


	public void setBrojOsoba(int brojOsoba) {
		this.brojOsoba = brojOsoba;
	}


	public LinijaDTO getLinija() {
		return linija;
	}


	public void setLinija(LinijaDTO linija) {
		this.linija = linija;
	}
	
	

}
