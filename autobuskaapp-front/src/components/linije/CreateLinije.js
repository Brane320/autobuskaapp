import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class CreateLinije extends React.Component {

    constructor(props){
        super(props);

    let linija = {
        brojMesta: 0,
        cenaKarte : 0,
        vremePolaska: "",
        destinacija: "",
        prevoznik : null
    }

    this.state = {linija: linija, prevoznici: []};
}
create = this.create.bind(this);

componentDidMount(){
    this.getPrevoznici();
}
getPrevoznici(){
    TestAxios.get('/prevoznici')
        .then(res => {
             // handle success
             console.log(res);
             this.setState({prevoznici: res.data});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa ocitavanjem prevoznika!');
        });
}

prevoznikSelectionChanged(e){
    // console.log(e);

    let prevoznikId = e.target.value;
    let prevoznik = this.state.prevoznici.find((prevoznikDTO) => prevoznikDTO.id == prevoznikId);
    console.log(prevoznik);

    let linija = this.state.linija;

    linija.prevoznik = prevoznik;

   
    this.setState({linija : linija});
}

valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let linija = this.state.linija;
    linija[name] = value;

    this.setState({linija: linija });
  }

renderPrevozniciOptions() {
    return this.state.prevoznici.map(prevoznik => {
        return (
          
            <option key={prevoznik.id} value={prevoznik.id}>
                
                {prevoznik.naziv}
            </option>
        )
    });
}

create(e) {
    let linija = this.state.linija;
    let linijaDTO = {
        brojMesta: linija.brojMesta,
        cenaKarte : linija.cenaKarte,
        vremePolaska: linija.vremePolaska,
        destinacija: linija.destinacija,
        prevoznik : linija.prevoznik

    }

    TestAxios.post('/linije', linijaDTO)
    .then(res => {
        console.log(res);

        alert('Uspesno dodata linija!');
        this.props.history.push("/linije");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno kreiranje linije!');
    });

}
render(){
    return (
        <>
        <Row>
        <Col></Col>
        <Col xs="14" sm="12" md="12">
        <Form>
            <Form.Label htmlFor="zbrojMesta">Broj mesta</Form.Label>
            <Form.Control type="number" id="zbrojMesta" name="brojMesta" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zCenaKarte">Cena karte</Form.Label>
            <Form.Control id="zCenaKarte" name="cenaKarte" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zDestinacija">Destinacija</Form.Label>
            <Form.Control  id="zDestinacija" name="destinacija" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zVremePolaska">Vreme polaska</Form.Label>
            <Form.Control  id="zVremePolaska" name="vremePolaska" onChange={(e)=>this.valueInputChanged(e)}/> <br/>

            <Form.Label htmlFor="pPrevoznik">Prevoznik</Form.Label>
            <InputGroup>
            <Form.Control as="select" id="pPrevoznik" name="prevoznik" onChange={(e)=>this.prevoznikSelectionChanged(e)}>
            <option>Izaberi prevoznika</option>
            {this.renderPrevozniciOptions()}
            </Form.Control>

            </InputGroup>
           

       
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Kreiraj</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}

}

export default CreateLinije;