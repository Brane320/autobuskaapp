import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup } from "react-bootstrap";

class EditLinije extends React.Component {

    constructor(props){
        super(props);

    let linija = {
        id : -1,
        brojMesta: 0,
        cenaKarte : 0,
        vremePolaska: "",
        destinacija: "",
        prevoznik : {}
    }

    this.state = {linija : linija, prevoznici: []};
}

componentDidMount(){
    this.getPrevoznici();
    this.getLinijaById(this.props.match.params.id);
    
 
}

getLinijaById(linijaId) {
    TestAxios.get('/linije/' + linijaId)
    .then(res => {
        // handle success
        console.log(res.data);
        let linija = {
            id : res.data.id,
            brojMesta: res.data.brojMesta,
            cenaKarte : res.data.cenaKarte,
            vremePolaska: res.data.vremePolaska,
            destinacija: res.data.destinacija,
            prevoznik : res.data.prevoznik
            
           
            


        }
        this.setState({linija : linija});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Linija nije dobijena')
     });
}

getPrevoznici(){
    TestAxios.get('/prevoznici')
        .then(res => {
             // handle success
             console.log(res);
             this.setState({prevoznici: res.data});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa ocitavanjem prevoznika!');
        });
}
renderPrevozniciOptions() {
    return this.state.prevoznici.map(prevoznik => {
        return (
          
            <option key={prevoznik.id} value={prevoznik.id}>
                
                {prevoznik.naziv}
            </option>
        )
    });
}

prevoznikSelectionChanged(e){
    // console.log(e);

    let prevoznikId = e.target.value;
    let prevoznik = this.state.prevoznici.find((prevoznikDTO) => prevoznikDTO.id == prevoznikId);
    console.log(prevoznik);

    let linija = this.state.linija;

    linija.prevoznik = prevoznik;

   
    this.setState({linija : linija});
}
valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let linija = this.state.linija;
    linija[name] = value;

    this.setState({ linija:linija});
  }

  edit(){
    let linija = this.state.linija;
    let linijaDTO = {
            id : linija.id,
            brojMesta: linija.brojMesta,
            cenaKarte : linija.cenaKarte,
            vremePolaska: linija.vremePolaska,
            destinacija: linija.destinacija,
            prevoznik : linija.prevoznik

        
        


    }
    TestAxios.put('/linije/' + this.state.linija.id, linijaDTO)
    .then(res => {
        console.log(res);

        alert('Uspesno izmenjena linija!');
        this.props.history.push("/linije");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesan edit');
    });
}
render(){
    return (
        
        <>
            <Row>
            <Col></Col>
            <Col xs="14" sm="12" md="12">
            <h1>Izmena autobuske linije</h1>
            <Form>
                <Form.Label htmlFor="zBrojMesta">Broj mesta</Form.Label>
                <Form.Control type="number" id="zbrojMesta" name="brojMesta" value={this.state.linija.brojMesta} onChange={(e) => this.valueInputChanged(e)}/> <br/>
                <Form.Label htmlFor="zCenaKarte" >Cena karte</Form.Label>
                <Form.Control id="zCenaKarte" name="cenaKarte"  value={this.state.linija.cenaKarte} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
                <Form.Label id="zvremePolaska">Vreme polaska</Form.Label>
                <Form.Control id="zvremePolaska" name="vremePolaska" value={this.state.linija.vremePolaska} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
                <Form.Label htmlFor="zDestinacija" >Destinacija</Form.Label>
                <Form.Control id="zDestinacija" name="destinacija"  value={this.state.linija.destinacija} onChange={(e)=>this.valueInputChanged(e)}/> <br/>

                <Form.Label htmlFor="sprint">Prevoznik</Form.Label>
                <InputGroup>
                <Form.Control as="select" id="pr" name="prevoznik" value={this.state.linija.prevoznik.id} onChange={(e)=>this.prevoznikSelectionChanged(e)}>
                {this.renderPrevozniciOptions()}
                </Form.Control>

                </InputGroup>

               
               
                <Button style={{ marginTop: "25px" }}onClick={() => this.edit()}>Izmeni</Button>
            </Form>
            </Col>

            
            <Col></Col>
            </Row>

    
          
     
        </>


    

    )
}

}



export default EditLinije;





