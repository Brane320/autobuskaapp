import React from 'react';
import {Table, Button, Form,Col,InputGroup,Row} from 'react-bootstrap'
import TestAxios from '../../apis/TestAxios';

class Linije extends React.Component {

    constructor(props) {
        super(props);

        

        this.state = { prevoznici : [],linije: [], brojStranice : 0,totalPages : 0, destinacija : null, maxCenaKarte : null,prevoznikId : null}
    }

    componentDidMount(){
        this.getLinije(0);
        this.getPrevoznike();
    }

    getPrevoznike(){
        TestAxios.get('/prevoznici')
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({prevoznici: res.data});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Problem sa ocitavanjem prevoznika!');
            });
    }

    pretraga() {
        var params = {
            'destinacija' : this.state.destinacija,
            'prevoznikId' : this.state.prevoznikId,
            'maxCenaKarte' : this.state.maxCenaKarte


        }

        TestAxios.get('/linije/pretraga', {params})
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({linije : res.data});
                 
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri pretrazi');
            });
    }

    prevoznikSelectionChanged(e){
        // console.log(e);

        let prevoznikId = e.target.value;
        let prevoznik = this.state.prevoznici.find((prevoznikDTO) => prevoznikDTO.id == prevoznikId);
        console.log(prevoznik);

       
        this.setState({prevoznikId : prevoznikId});
    }

    destinacijaInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;

        console.log(name + ", " + value);

        let destinacija = this.state.destinacija;

        destinacija = value;

        this.setState({destinacija : destinacija})
        
    
   
        console.log(destinacija);
      }

      maxCenaInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;

        console.log(name + ", " + value);

        let maxCenaKarte = this.state.maxCenaKarte;

        maxCenaKarte = value;

        this.setState({maxCenaKarte : maxCenaKarte})
        
    
   
        console.log(maxCenaKarte);
      }





getLinije(brojStranice) {
    let config = {
        params: {
          brojStranice: this.state.brojStranice,
          brojStranice : brojStranice
        },
      }
    TestAxios.get('/linije', config)
        .then(res => {
             // handle success
             console.log(res);
             this.setState({brojStranice : brojStranice, linije: res.data,  totalPages : res.headers["total-pages"]});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Greska pri ocitavanju linija');
        });
}
renderPrevozniciOptions() {
    return this.state.prevoznici.map(prevoznik => {
        return (
          
            <option key={prevoznik.id} value={prevoznik.id}>
                
                {prevoznik.naziv}
            </option>
        )
    });
}

goToCreate(){
    this.props.history.push('/createliniju');
}

goToEdit(id){
    this.props.history.push('/editlinije/' + id);
}

goToRezervisi(id){
    this.props.history.push('/rezervisi/' + id);
}
delete(id) {
    TestAxios.delete('/linije/' + id)
    .then(res => {
        // handle success
        console.log(res);
        alert('Linija uspesno obrisana');
        window.location.reload();
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Problem sa brisanjem ');
     });
}

renderLinije() {
    return this.state.linije.map((linija) => {
        return (
           <tr key={linija.id} className = "table-info">
              <td>{linija.prevoznik.naziv}</td>
              <td>{linija.destinacija}</td>
              <td>{linija.brojMesta}</td>
              <td>{linija.vremePolaska}</td>
              <td>{linija.cenaKarte}</td>
              <td> <Button disabled={linija.brojMesta == 0} onClick={() => this.goToRezervisi(linija.id)}>Rezervisi</Button>
              <Button style={{margin:3}} variant="warning" onClick={() => this.goToEdit(linija.id)}>Izmeni</Button>
              <Button style={{margin:3}} variant="danger" onClick={() => this.delete(linija.id)}>Obrisi</Button></td>

           
           </tr>
        )
     })
}
render() {
     
    return (
        <div>
              <div>
            <h1>Pretraga autobuskih linija</h1>
        
            <Col></Col>
            <Col xs="14" sm="12" md="12">
            <Form>
                <Form.Label htmlFor="dest">Destinacija</Form.Label>
                <Form.Control id="dest" name="destinacija" placeHolder="Upisite destinaciju" onChange={(e)=>this.destinacijaInputChanged(e)}/> <br/>

                
                <Form.Label htmlFor="prev">Prevoznik</Form.Label>
                <InputGroup>
                <Form.Control as="select" id="prev" name="prevoznik" onChange={e => this.prevoznikSelectionChanged(e)}> 
                <option>Odaberite prevoznika</option>
                {this.renderPrevozniciOptions()}
                
                </Form.Control>
                </InputGroup>
                <br/>
                <Form.Label htmlFor="max">Maksimalna cena</Form.Label>
                <Form.Control placeholder="Upisite maksimalnu cenu" id="max" name="maxCena" onChange={(e)=>this.maxCenaInputChanged(e)}/> <br/>
                
               
                <Button style={{ marginTop: "25px" }}onClick={(e) => this.pretraga()}>Pretrazi</Button>
            </Form>
            </Col>
        </div>
        <div>
                    
        <br/>
        <div style={{float:"left"}}>  
        <Button onClick={(e) => this.goToCreate()}>Kreiraj novu liniju</Button>
        </div>
        <div style={{float:"right"}}><Button disabled={this.state.brojStranice==0} onClick={()=>this.getLinije(this.state.brojStranice -1)}>Prethodna stranica</Button>
      
      <Button disabled={this.state.brojStranice==this.state.totalPages-1} onClick={()=>this.getLinije(this.state.brojStranice+1)}>Sledeća stranica</Button>
  </div>
        
        <Table className="primary" style={{marginTop:5}}>
            <thead>
                <tr  className="bg-primary">
                    <th>Naziv prevoznika</th>
                    <th>Destinacija</th>
                    <th>Broj mesta</th>
                    <th>Vreme polaska</th>
                    <th>Cena karte</th>
                    
                    <th></th>
                    
                    
                    
                    
                    
                </tr>
            </thead>
            <tbody>
                {this.renderLinije()}
            </tbody>                  
        </Table>
        
    </div>
  

</div>

);
}

}
export default Linije;