import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class Rezervisi extends React.Component {

    constructor(props){
        super(props);

    let linija = {
        id : -1,
        brojMesta: 0,
        cenaKarte : 0,
        vremePolaska: "",
        destinacija: "",
        prevoznik : {}
    }

    this.state = {brojOsoba : 0, linija : linija};
}

    //edit = this.edit.bind(this)

    componentDidMount(){
        this.getLinijaById(this.props.match.params.id);
        
     
    }

    getLinijaById(linijaId) {
        TestAxios.get('/linije/' + linijaId)
        .then(res => {
            // handle success
            console.log(res.data);
            let linija = {
                id : res.data.id,
                brojMesta: res.data.brojMesta,
                cenaKarte : res.data.cenaKarte,
                vremePolaska: res.data.vremePolaska,
                destinacija: res.data.destinacija,
                prevoznik : res.data.prevoznik
                
               
                
    
    
            }
            this.setState({linija : linija});
            
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Linija nije dobijena')
         });
    }

    brojOsobaInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;

        console.log(name + ", " + value);
    
        let brojOsoba = this.state.brojOsoba;
        brojOsoba = value;
    
        this.setState({ brojOsoba : brojOsoba});
      }

      create(e){
        
    
        
        let rezervacijaDTO = {
            brojOsoba : this.state.brojOsoba,
            linija : this.state.linija
            


        }

       
        TestAxios.post('/rezervacije' , rezervacijaDTO)
        .then(res => {
            console.log(res);

            alert('Uspesna rezervacija karte');
            this.props.history.push('/linije');
        
        })

        .catch(error => {
            console.log(error);

            alert('Error! Neuspesna rezervacija');
        });
    }

    render(){
        return (
            <>
            <Row>
                <h1>Izabrali ste liniju destinacije {this.state.linija.destinacija}. Vreme polaska je {this.state.linija.vremePolaska}.</h1>
                
                
            <Col></Col>
            <Col xs="14" sm="12" md="12">
            <Form>
              
                <Form.Label id="zBrojOsoba">Broj osoba</Form.Label>
                <Form.Control type="number" placeHolder="Ovde unesite broj osoba vase rezervacije" id="zBrojOsoba" name="brojOsoba" onChange={(e)=>this.brojOsobaInputChanged(e)}/> <br/>
               
    
               
    
                <br/>
                
               
         
                <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Rezervisi kartu</Button>
            </Form>
            </Col>
    
            
            <Col></Col>
            </Row>
    
    
          
     
        </>
    
        
    
        )
    }

}

export default Rezervisi;