import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, Container,Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/login/Login"
import Register from "./components/register/Register"
import Linije from "./components/linije/Linije"
import {logout} from './services/auth'
import './index.css'
import Home from './Home';
import CreateLinije from './components/linije/CreateLinije';
import EditLinije from './components/linije/EditLinije';
import Rezervisi from './components/rezervacije/Rezervisi';





class App extends React.Component {
  

    render() {
        return (
            
          <div>
          <Router>
            <Navbar expand bg="primary" variant="dark">
              <Navbar.Brand as={Link} to="/">
                  Pocetna
              </Navbar.Brand>
             
              
              <Nav>
                <Nav.Link as={Link} to="/linije">
                  Prikaz autobuskih linija
                </Nav.Link>

                {window.localStorage['jwt'] ? 
                          <Button hidden onClick = {()=>logout()}>Logout</Button> :
                          <Nav.Link as={Link} to="/register">Registracija</Nav.Link>
                          }


                

                
               
                {window.localStorage['jwt'] ? 
                          <Button onClick = {()=>logout()}>Logout</Button> :
                          <Nav.Link as={Link} to="/login">Login</Nav.Link>
                          }
              </Nav>
            </Navbar>
              <Container style={{paddingTop:"25px"}}>
                <Switch>
                  
                <Route exact path="/" component={Home}/> 
                <Route exact path="/login" component={Login}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/linije" component={Linije}/>
                <Route exact path="/createliniju" component={CreateLinije}/>
                <Route exact path="/editlinije/:id" component={EditLinije}/>
                <Route exact path="/rezervisi/:id" component={Rezervisi}/>

                

               
                  
                  
                </Switch>
              </Container>
            </Router>
            
          </div>
        )
    }
};

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);