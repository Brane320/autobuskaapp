
import TestAxios from "../apis/TestAxios"

export const login = async function(username, password){
    const data = {
        username: username,
        password: password
    }
    try{
        const ret = await TestAxios.post('korisnici/auth', data);
        window.localStorage.setItem('jwt', ret.data);
        window.localStorage.setItem('uloga',ret.data.claims.role);
        console.log(ret.data.claims.role);
        
        console.log('Uspesno logovanje');
    }catch(error){
        console.log(error);
    }
    window.location.reload();
}

export const logout = function(){
    window.localStorage.removeItem('jwt');
    window.location.reload();
}